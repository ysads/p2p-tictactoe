#define uchar unsigned char

#define PACKET_SEPARATOR    0x00
#define SEPARATORS_STR      "\x1F\n"
#define UNIT_SEPARATOR      0x1F

// Limits
#define MAX_CLIENTS         16
#define MAX_LINE            28
#define MAX_LISTEN_QUEUE    8
#define MAX_PACKET          257
#define MAX_PASSWORD        11
#define MAX_SCORE           4
#define MAX_USERNAME        11
#define MAX_USERS           16

// Statuses
#define FAILURE             0
#define PEER_DISCONNECTED   1
#define SUCCESS             2
#define UNKNOWN_USER        3
#define WRONG_PASSWORD      4

// Statuses literals
#define S_FAILURE           "0"
#define S_PEER_DISCONNECTED "1"
#define S_SUCCESS           "2"
#define S_UNKNOWN_USER      "3"
#define S_WRONG_PASSWORD    "4"

// Packet codes from server to client
#define LEADERS_RESP        0x01
#define LIST_RESP           0x02
#define LOGIN_RESP          0x03
#define PASSWD_RESP         0x04
#define PING_REQ            0x05

// Packet codes from client to server
#define ADD_USER            0x06
#define LEADERS_REQ         0x07
#define LIST_REQ            0x08
#define LOGIN_REQ           0x09
#define LOGOUT              0x0A
#define MATCH_BEGIN         0x0B
#define MATCH_END           0x0C
#define PASSWD_REQ          0x0D
#define PING_RESP           0x0E

// Packet codes from peer to peer
#define BEGIN_REQ           0x0F
#define BEGIN_RESP          0x10
#define END                 0x11
#define PEER_PING_REQ       0x12
#define PEER_PING_RESP      0x13
#define SEND                0x14

struct notification_args {
    int port;
    int seconds;
};

void add_padding(char **, int);
int connect_to_host(char *, int);
int create_notifier(int, int, int);
int get_listenfd(int);
void get_next_payload(char *, char *);
uchar get_packet_type(char *);
char *get_type_str(uchar);
void *notify(void *);
void send_packet(int, uchar, int, ...);
