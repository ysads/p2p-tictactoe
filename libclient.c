#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/time.h>

#include "lib.h"
#include "libclient.h"

struct peer pool[MAX_CLIENTS];
int n_pool = 0;

struct timeval start;
double latencies[3] = {0, 0, 0};
int moves_played;
int board[3][3] = {{SYM_EMPTY, SYM_EMPTY, SYM_EMPTY},
                   {SYM_EMPTY, SYM_EMPTY, SYM_EMPTY},
                   {SYM_EMPTY, SYM_EMPTY, SYM_EMPTY}};

void get_next_arg(char *arg)
{
    char *next = strtok(NULL, " \n");
    memcpy(arg, next, strlen(next) + 1);
}

uchar get_command_type(char *command)
{
    char *type = strtok(command, " \n");

    if (strcmp(type, "adduser") == 0)
    {
        return C_ADDUSER;
    }
    if (strcmp(type, "begin") == 0)
    {
        return C_BEGIN;
    }
    if (strcmp(type, "delay") == 0)
    {
        return C_DELAY;
    }
    if (strcmp(type, "end") == 0)
    {
        return C_END;
    }
    if (strcmp(type, "exit") == 0)
    {
        return C_EXIT;
    }
    if (strcmp(type, "leaders") == 0)
    {
        return C_LEADERS;
    }
    if (strcmp(type, "list") == 0)
    {
        return C_LIST;
    }
    if (strcmp(type, "login") == 0)
    {
        return C_LOGIN;
    }
    if (strcmp(type, "logout") == 0)
    {
        return C_LOGOUT;
    }
    if (strcmp(type, "passwd") == 0)
    {
        return C_PASSWD;
    }
    if (strcmp(type, "send") == 0)
    {
        return C_SEND;
    }
    return C_UNKNOWN;
}

void get_peer_by_username(char *username, struct peer **peer)
{
    for (int i = 0; i < n_pool; i++)
    {
        if (strcmp(pool[i].username, username) == 0)
        {
            *peer = &pool[i];
            return;
        }
    }
    *peer = NULL; // No peer found with this username
}

void prompt(void)
{
    printf("JogoDaVelha> ");
    fflush(stdout);
}

void save_users_list(char *buffer)
{
    char username[MAX_USERNAME];
    char ip[INET_ADDRSTRLEN];
    char is_playing_str[2];
    int is_playing;
    char ch;
    int j, ptr = 0;

    for (int i = 0; i < n_pool; i++)
    {
        j = 0;

        for (; (ch = buffer[ptr]) != UNIT_SEPARATOR; j++, ptr++)
        {
            username[j] = buffer[ptr];
        }
        username[j] = '\0';
        ptr++;
        j = 0;

        for (; (ch = buffer[ptr]) != UNIT_SEPARATOR; j++, ptr++)
        {
            ip[j] = buffer[ptr];
        }
        ip[j] = '\0';
        ptr++;

        is_playing_str[0] = buffer[ptr];
        is_playing_str[1] = '\0';
        is_playing = atoi(is_playing_str);
        ptr++;

        strcpy(pool[i].username, username);
        strcpy(pool[i].ip, ip);
        pool[i].is_playing = is_playing;
    }
}

void show_users_list()
{
    char *msg_1 = "in a match";
    char *msg_2 = "free";
    char *username_copy = malloc(MAX_PASSWORD);

    for (int i = 0; i < n_pool; i++)
    {
        strcpy(username_copy, pool[i].username);
        add_padding(&username_copy, MAX_USERNAME);
        printf("%s ", username_copy);

        if (pool[i].is_playing)
        {
            printf("%s\n", msg_1);
        }
        else
        {
            printf("%s\n", msg_2);
        }
    }
    free(username_copy);
}

void show_users_scores(char *buffer, int n_records)
{
    char *username;
    char *score;

    username = strtok(buffer, SEPARATORS_STR);
    score = strtok(NULL, SEPARATORS_STR);

    for (int i = 0; i < n_records; i++)
    {
        printf("%s %s\n", username, score);

        username = strtok(NULL, SEPARATORS_STR);
        score = strtok(NULL, SEPARATORS_STR);
    }
}

void start_timer()
{
    gettimeofday(&start, NULL);
}

void register_latency()
{
    double latency;
    struct timeval stop;

    gettimeofday(&stop, NULL);
    latency = 1000.0 * (stop.tv_sec - start.tv_sec);
    latency += (stop.tv_usec - start.tv_usec) / 1000.0;

    latencies[2] = latencies[1];
    latencies[1] = latencies[0];
    latencies[0] = latency;
}

void print_board()
{
    char symbol;

    printf("Board state: \n");
    for (int i = 0; i < 3; i++)
    {
        printf("| ");
        for (int j = 0; j < 3; j++)
        {
            if (board[i][j] == SYM_X)
            {
                symbol = 'X';
            }
            else if (board[i][j] == SYM_O)
            {
                symbol = '0';
            }
            else
            {
                symbol = ' ';
            }
            printf("%c | ", symbol);
        }
        printf("\n");
    }
}

void init_board()
{
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            board[i][j] = SYM_EMPTY;
        }
    }
}

void update_board(char* updated_board)
{
    int row, col;

    for (int i = 0; i < 9; i++)
    {
        row = i / 3;
        col = i % 3;

        board[row][col] = updated_board[i] - '0';
    }
}

void serialize_board(char* serialized_board)
{
    int row, col;

    for (int i = 0; i < 9; i++) {
        row = i / 3;
        col = i % 3;

        serialized_board[i] = '0' + board[row][col];
    }
}

int play_move(struct client_state* client, char* row, char* col)
{
    int row_int, col_int;

    row_int = atoi(row);
    col_int = atoi(col);

    if (row_int < 0 || row_int >= 3)
    {
        return 0;
    }
    if (col_int < 0 || col_int >= 3)
    {
        return 0;
    }
    if (board[row_int][col_int] != SYM_EMPTY)
    {
        return 0;
    }

    board[row_int][col_int] = client->symbol;

    return 1;
}

int find_winner()
{
    int score_v, score_h;
    int score_d1, score_d2;

    // Check horizontal and vertical lines
    for (int i = 0; i < 3; i++)
    {
        score_v = score_h = 0;

        for (int j = 0; j < 3; j++)
        {
            score_h += board[i][j];
            score_v += board[j][i];
        }

        if (score_v == WINNER_X || score_h == WINNER_X)
        {
            return SYM_X;
        }
        if (score_v == WINNER_O || score_h == WINNER_O)
        {
            return SYM_O;
        }
    }

    // Check both diagonals
    score_d1 = score_d2 = 0;
    for (int i = 0; i < 3; i++)
    {
        score_d1 += board[i][i];
        score_d2 += board[i][2-i];
    }
    if (score_d1 == WINNER_X || score_d2 == WINNER_X)
    {
        return SYM_X;
    }
    if (score_d1 == WINNER_O || score_d2 == WINNER_O)
    {
        return SYM_O;
    }
    return 0;
}