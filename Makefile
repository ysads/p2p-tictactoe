CC=gcc
CFLAGS=-Wall
DEPS=lib.h libclient.h libserver.h

all: client server clean

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

client: client.o lib.o libclient.o
	$(CC) -pthread -o client client.o lib.o libclient.o

server: server.o lib.o libserver.o
	$(CC) -pthread -o server server.o lib.o libserver.o

clean:
	rm *.o
