#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>

#include "lib.h"
#include "libserver.h"

FILE *events, *users;
struct client clients[MAX_CLIENTS];
int n_clients = 0;
int n_logged = 0;

void add_user(char *username, char *password)
{
    char score[] = "0  ";

    users = fopen("users.ttt", "a");
    add_padding(&username, MAX_USERNAME);
    add_padding(&password, MAX_PASSWORD);
    fprintf(users, "%s%c%s%c%s\n", username, UNIT_SEPARATOR, password, UNIT_SEPARATOR, score);
    fflush(users);
    fclose(users);
}

void update_password(struct client *client, char *new_password)
{
    char *db_username;
    char *username_copy = malloc(MAX_USERNAME);
    char db_line[MAX_LINE + 1];
    long ptr;

    users = fopen("users.ttt", "r+");

    strcpy(username_copy, client->username);
    add_padding(&username_copy, MAX_USERNAME);

    while (1)
    {
        fgets(db_line, MAX_LINE, users);
        ptr = ftell(users) - MAX_PASSWORD - MAX_SCORE;
        db_username = strtok(db_line, SEPARATORS_STR);

        // If usernames match, then update the score
        if (strcmp(db_username, username_copy) == 0)
        {
            add_padding(&new_password, MAX_PASSWORD);
            fseek(users, ptr, SEEK_SET);
            fprintf(users, "%s", new_password);
            fflush(users);
            fclose(users);
            free(username_copy);
            break;
        }
    }
}

void update_score(char *username, int score)
{
    char *db_username, *db_score, *new_score_str;
    char *username_copy = malloc(MAX_USERNAME);
    char db_line[MAX_LINE + 1];
    int new_score;
    long ptr;

    users = fopen("users.ttt", "r+");
    new_score_str = malloc(MAX_SCORE);

    strcpy(username_copy, username);
    add_padding(&username_copy, MAX_USERNAME);

    while (1)
    {
        fgets(db_line, MAX_LINE, users);
        ptr = ftell(users) - MAX_SCORE;
        db_username = strtok(db_line, SEPARATORS_STR);
        strtok(NULL, SEPARATORS_STR);
        db_score = strtok(NULL, SEPARATORS_STR);

        // If usernames match, then update the score
        if (strcmp(db_username, username_copy) == 0)
        {
            new_score = atoi(db_score) + score;
            sprintf(new_score_str, "%d", new_score);
            add_padding(&new_score_str, MAX_SCORE);
            fseek(users, ptr, SEEK_SET);
            fprintf(users, "%s", new_score_str);

            free(new_score_str);
            fflush(users);
            fclose(users);
            break;
        }
    }
}

void client_init(int fd, char *ip)
{
    clients[n_clients].fd = fd;
    clients[n_clients].is_logged_in = 0;
    clients[n_clients].is_playing = 0;
    clients[n_clients].n_pings_unanswered = 0;
    strcpy(clients[n_clients].ip, ip);

    n_clients++;
}

void client_login(int fd, char *username, char *password)
{
    for (int i = 0; i < n_clients; i++)
    {
        if (clients[i].fd == fd)
        {
            clients[i].is_logged_in = 1;
            strcpy(clients[i].username, username);
            strcpy(clients[i].password, password);
            n_logged++;
            break;
        }
    }
}

void client_logout(struct client *client)
{
    client->is_logged_in = 0;
    n_logged--;
}

void client_remove(int fd)
{
    for (int i = 0; i < n_clients; i++)
    {
        if (clients[i].fd == fd)
        {
            // We only need to shift positions on the array if the item to remove is not the last one.
            // Also, prevents error by using strcpy with both args pointing to same memory area.
            if (i != n_clients-1)
            {
                // Deep copy the structs
                clients[i].fd = clients[n_clients - 1].fd;
                clients[i].is_logged_in = clients[n_clients - 1].is_logged_in;
                clients[i].is_playing = clients[n_clients - 1].is_playing;
                clients[i].n_pings_unanswered = clients[n_clients - 1].n_pings_unanswered;
                strcpy(clients[i].ip, clients[n_clients - 1].ip);
                strcpy(clients[i].opponent_ip, clients[n_clients - 1].opponent_ip);
                strcpy(clients[i].opponent_username, clients[n_clients - 1].opponent_username);
                strcpy(clients[i].username, clients[n_clients - 1].username);
                strcpy(clients[i].password, clients[n_clients - 1].password);
            }
            n_clients--;
            break;
        }
    }
}

void get_client(int fd, struct client **client)
{
    for (int i = 0; i < n_clients; i++)
    {
        if (clients[i].fd == fd)
        {
            *client = &clients[i];
            break;
        }
    }
}

void get_client_by_username(char *username, struct client **client)
{
    for (int i = 0; i < n_clients; i++)
    {
        if (strcmp(clients[i].username, username) == 0)
        {
            *client = &clients[i];
            break;
        }
    }
}

void get_ip(int fd, char *ip)
{
    struct sockaddr_in remote_addr;
    socklen_t addr_len = sizeof(remote_addr);

    getpeername(fd, (struct sockaddr *)&remote_addr, &addr_len);
    strcpy(ip, inet_ntoa(remote_addr.sin_addr));
}

void get_username(int fd, char *username)
{
    for (int i = 0; i < n_clients; i++)
    {
        if (clients[i].fd == fd)
        {
            strcpy(username, clients[i].username);
        }
    }
}

void log_and_shutdown(int s)
{
    log_event(SERVER_STOPPED, 1, "SIGINT received");
    exit(0);
}

void log_event(uchar event_type, int n_args, ...)
{
    char *time_str;
    time_t rawtime;
    struct tm *timeinfo;
    va_list args_list;
    va_start(args_list, n_args);

    time(&rawtime);
    timeinfo = localtime(&rawtime);
    time_str = asctime(timeinfo);
    time_str[24] = '\0';

    events = fopen("events.log", "a");
    fprintf(events, "| %s | ", time_str);

    switch (event_type)
    {
        case CLIENT_CONNECTED:
        {
            fprintf(events, "CLIENT CONNECTED | ");
            break;
        }
        case CLIENT_DISCONNECTED:
        {
            fprintf(events, "CLIENT DISCONNECTED | ");
            break;
        }
        case CLIENT_LOGIN:
        {
            fprintf(events, "CLIENT LOGIN | ");
            break;
        }
        case CLIENT_LOGOUT:
        {
            fprintf(events, "CLIENT LOGOUT | ");
            break;
        }
        case MATCH_ENDED:
        {
            fprintf(events, "MATCH ENDED | ");
            break;
        }
        case MATCH_STARTED:
        {
            fprintf(events, "MATCH STARTED | ");
            break;
        }
        case SERVER_STARTED:
        {
            fprintf(events, "SERVER STARTED | ");
            break;
        }
        case SERVER_STOPPED:
        {
            fprintf(events, "SERVER STOPPED | ");
            break;
        }
    }

    for (int i = 0; i < n_args; i++)
    {
        fprintf(events, "%s | ", va_arg(args_list, char *));
    }

    va_end(args_list);
    fprintf(events, "\n");
    fflush(events);
    fclose(events);
}

void stringify_logged_users(char *buffer)
{
    int ptr = 0;
    char is_playing_str[2];

    for (int i = 0; i < n_clients; i++)
    {
        if (clients[i].is_logged_in)
        {
            for (int j = 0; j < strlen(clients[i].username); j++, ptr++)
            {
                buffer[ptr] = clients[i].username[j];
            }
            buffer[ptr] = UNIT_SEPARATOR;
            ptr++;

            for (int j = 0; j < strlen(clients[i].ip); j++, ptr++)
            {
                buffer[ptr] = clients[i].ip[j];
            }
            buffer[ptr] = UNIT_SEPARATOR;
            ptr++;

            sprintf(is_playing_str, "%d", clients[i].is_playing);
            buffer[ptr] = is_playing_str[0];
            ptr++;

            if (i < n_clients - 1)
            {
                buffer[ptr] = UNIT_SEPARATOR;
            }
        }
    }
}


void stringify_leaders(char *buffer, char *n_str)
{
    char *db_username, *db_score;
    char db_line[MAX_LINE + 1];
    int n_records = 0;
    int ptr = 0;

    users = fopen("users.ttt", "r");

    while (1)
    {
        fgets(db_line, MAX_LINE, users);

        // The only way to check feof is *after* reading the EOF char
        if (feof(users))
        {
            break;
        }

        n_records++;

        db_username = strtok(db_line, SEPARATORS_STR);
        strtok(NULL, SEPARATORS_STR);
        db_score = strtok(NULL, SEPARATORS_STR);

        // Serialize username
        for (int i = 0; i < strlen(db_username); i++, ptr++)
        {
            buffer[ptr] = db_username[i];
        }
        buffer[ptr] = UNIT_SEPARATOR;
        ptr++;

        // Serialize score
        for (int i = 0; i < strlen(db_score); i++, ptr++)
        {
            buffer[ptr] = db_score[i];
        }
        buffer[ptr] = UNIT_SEPARATOR;
        ptr++;
    }
    sprintf(n_str, "%d", n_records);
    fclose(users);
}

int verify_user(char *username, char *password)
{
    char *db_username, *db_password;
    char db_line[MAX_LINE + 1];
    char *username_copy = malloc(MAX_USERNAME);
    char *password_copy = malloc(MAX_PASSWORD);

    /* make shure the file exists */
    users = fopen("users.ttt", "a");
    fclose(users);
    users = fopen("users.ttt", "r");

    strcpy(username_copy, username);
    strcpy(password_copy, password);
    add_padding(&username_copy, MAX_USERNAME);
    add_padding(&password_copy, MAX_PASSWORD);

    while (1)
    {
        fgets(db_line, MAX_LINE, users);

        if (feof(users))
        {
            break;
        }
        db_username = strtok(db_line, SEPARATORS_STR);
        db_password = strtok(NULL, SEPARATORS_STR);

        // If usernames match, then check the passwords
        if (strcmp(db_username, username_copy) == 0)
        {
            if (strcmp(db_password, password_copy) == 0)
            {
                fclose(users);
                return SUCCESS;
            }
            else
            {
                fclose(users);
                return WRONG_PASSWORD;
            }
        }
    }
    free(username_copy);
    free(password_copy);
    fclose(users);
    return UNKNOWN_USER;
}
