#define N_RESERVED_FDS          2

// Limits
#define MAX_UNANSWERED_PINGS    3

// Scores
#define SCORE_TIE               1
#define SCORE_WIN               3

// Event types
#define CLIENT_CONNECTED        0x01
#define CLIENT_DISCONNECTED     0x02
#define CLIENT_LOGIN            0x03
#define CLIENT_LOGOUT           0x04
#define MATCH_ENDED             0x05
#define MATCH_STARTED           0x06
#define SERVER_STARTED          0x07
#define SERVER_STOPPED          0x08

struct client {
    int fd;
    int is_logged_in;
    int is_playing;
    int n_pings_unanswered;
    char ip[INET_ADDRSTRLEN];
    char opponent_ip[INET_ADDRSTRLEN];
    char opponent_username[MAX_USERNAME];
    char username[MAX_USERNAME];
    char password[MAX_PASSWORD];
};

extern int n_clients;
extern int n_logged;
extern struct client clients[MAX_CLIENTS];

void add_user(char *, char *);
void update_password(struct client *, char *);
void update_score(char *, int);
void client_init(int, char *);
void client_login(int, char *, char *);
void client_logout(struct client *);
void client_remove(int);
void get_ip(int, char *);
void get_client(int, struct client **);
void get_client_by_username(char *, struct client **);
void get_username(int, char *);
void log_and_shutdown(int);
void log_event(uchar, int, ...);
void stringify_logged_users(char *);
void stringify_leaders(char *, char *);
int verify_user(char *, char *);
