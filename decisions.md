# Decisões

* `adduser` assume que os nomes de usuário são únicos

* nomes de usuário têm no máx 10 caracteres
* passwords têm no máximo 10 caracteres
* pacotes têm no máximo 100 caracteres
* no máximo 99 users salvos no banco
* scores têm no máximo 3 caracteres – ie até 999

* quem digitou `begin` avisa ao servidor do início da partida
* quem digitou `end` avisa ao servidor do fim da partida

* se um cliente cai durante uma partida, o servidor encerra a partida para os 2 peers
* se o servidor cair, o cliente inicia um timer; se der 3 min, avisa o usuário

* tabuleiro indexado de 0 a 2 tanto em linha quanto em coluna

## Struct do servidor para manter clientes

```C
struct client {
    int fd;
    int is_logged_in;
    int is_playing;
    int n_pings_unanswered;
    char ip[INET_ADDRSTRLEN];
    char opponent_ip[INET_ADDRSTRLEN];
    char opponent_username[MAX_USERNAME];
    char username[MAX_USERNAME];
    char password[MAX_PASSWORD];
};
```

## Ciclo de vida do servidor

* servidor iniciado
    - ✅ log

* servidor encerrado
    - ✅ log

## Ciclo de vida de clientes

* servidor aceita conexão de cliente
    - ✅ servidor inicializa o cliente no array clients
    - ✅ log com IP

* servidor identifica socket pendurado
    - ✅ localiza o usuário em clients
    - ✅ sobrescreve com o último cliente do array
    - ✅ decrementa n_clients
    - ✅ decrementa n_logged se estava setado
    - ✅ log com IP

## Ping

* cliente e servidor possuem clock notificador numa thread
* servidor notificado:
    - ✅ envia PING_REQ para todos os clientes
    - ✅ servidor incrementa n_pings_unanswered para cada cliente
    - se n_pings_unanswered > threshold
        - log com status, IP
    - ✅ cliente envia PING_RESP para servidor
    - ✅ servidor reseta n_pings_unanswered para este cliente
* peer_1 notificado:
    - ✅ se estiver numa partida:
        - ✅ peer_1 envia PEER_PING_REQ para peer_2
        - ✅ peer_1 inicia um timer
        - ✅ peer_2 envia PEER_PING_RESP para peer_1
        - ✅ peer_1 encerra o timer
        - ✅ peer_1 move valores do array latency para frente, liberando posição 0
        - ✅ peer_1 salva latência na posição 0 do array latency

## Comandos de usuário

* adduser
    - ✅ cliente envia para o servidor:
        - ✅ ADD_USER
        - ✅ username
        - ✅ password
    - ✅ servidor registra em users.ttt

* begin
    - ✅ não permitido pelo cliente se está numa partida
    - ✅ não permitido pelo cliente se não está logado
    - ✅ peer_1 busca IP do peer_2 pelo username em pool
    - ✅ se peer não localizado, não tenta iniciar a partida
    - ✅ peer_1 conecta com peer_2
    - ✅ peer_1 envia para o peer_2:
        - ✅ BEGIN_REQ
        - ✅ username
    - ✅ peer_1 fica sem prompt enquanto não tem resposta
    - ✅ peer_2 exibe mensagem que peer_1 quer iniciar uma partida
    - ✅ usuário do peer_2 entra com y/n
    - se y:
        - ✅ peer_2 envia para o peer_1:
            - ✅ BEGIN_RESP
            - ✅ SUCCESS
            - ✅ username
        - ✅ peer_1 seta is_playing
        - ✅ peer_1 seta opponent_username
        - ✅ peer_2 seta is_playing
        - ✅ peer_2 seta opponent_username
        - ✅ peer_1 envia para o servidor:
            - ✅ MATCH_BEGIN
            - ✅ username
            - ✅ opponent_username
        - ✅ servidor atualiza is_playin dos peers
        - ✅ servidor atualiza opponent_username dos peers
        - ✅ log com usernames e IPs do peer_1 e do peer_2
        - ✅ ambos os peers inicializam o tabuleiro com vazio em todas as posições
        - ✅ peer_1 inicializa my_symbol com X
        - ✅ peer_2 inicializa my_symbol com O
    - ✅ se n:
        - ✅ peer_2 envia para o peer_1:
            - ✅ BEGIN_RESP
            - ✅ FAILURE
        - ✅ peer_1 fecha o socket
        - ✅ peer_2 fecha o socket

* delay
    - ✅ não permitido pelo cliente se não está numa partida
    - ✅ itera pelo array latency exibindo todos os 3 valores

* end
    - ✅ não permitido pelo cliente se não está numa partida
    - ✅ peer_1 envia END para peer_2
    - ✅ peer_1 fecha o socket
    - ✅ peer_2 fecha o socket
    - ✅ peer_1 envia para o servidor:
        - ✅ MATCH_END
        - ✅ FAILURE
        - ✅ username
        - ✅ opponent_username
    - ✅ servidor atualiza is_playin dos peers
    - ✅ log com usernames e IPs do peer_1 e do peer_2
    - ✅ log com o nome do vencedor
    - ✅ se username é TIE, atualiza ambos os users com +1 de score
    - ✅ senão atualiza somente o vencedor com +3

* exit
    - ✅ não permitido pelo cliente se está numa partida
    - ✅ não permitido pelo cliente se está logado
    - ✅ não envia nenhum pacote, pois desconexão é identificada por socket pendurado

* leaders
    - ✅ cliente envia LEADERS_REQ para o servidor
    - ✅ servidor itera por users.ttt salvando pares (username, score) num buffer
    - ✅ salva tudo em um buffer
    - ✅ servidor envia para o cliente:
        - ✅ LEADERS_RESP
        - ✅ o buffer

* list
    - ✅ cliente envia LIST_REQ para o servidor
    - ✅ servidor itera pelo array clients salvando num buffer:
        - ✅ username
        - ✅ IP
        - ✅ is_playing
    - ✅ servidor envia LIST_RESP para o cliente com os dados do buffer
    - ✅ cliente salva (username, IP, is_playing) em pool

* login
    - ✅ não permitido pelo cliente se está logado
    - comunicação relizada com SSL
    - ✅ cliente envia para o servidor:
        - ✅ LOGIN_REQ
        - ✅ username
        - ✅ password
    - ✅ servidor busca username em users.ttt
    - ✅ se não existir, envia LOGIN_RESP com UNKNOWN_USER para o cliente
    - ✅ se a senha estiver errada, envia LOGIN_RESP com WRONG_PASSWORD para o cliente
    - ✅ se estiver correto, envia LOGIN_RESP com SUCCESS
    - ✅ servidor atualiza o array clients:
        - ✅ is_logged_in = 1
        - ✅ username
        - ✅ password
    - ✅ log com status, username, IP

* logout
    - ✅ não permitido pelo cliente se está numa partida
    - ✅ não permitido pelo cliente se não está logado
    - ✅ cliente reseta is_logged_in
    - ✅ cliente envia LOGOUT para o servidor
    - ✅ servidor atualiza o array clients:
        - ✅ is_logged_in = 0
    - ✅ log com username, IP

* passwd
    - ✅ não permitido pelo cliente se não está logado
    - comunicação relizada com SSL
    - ✅ cliente envia para o servidor:
        - ✅ PASSWD_REQ
        - ✅ old_password
        - ✅ new_password
    - ✅ servidor encontra fd do usuário em clients
    - ✅ compara as senhas
    - ✅ servidor envia para o cliente:
        - ✅ PASSWD_RESP
        - ✅ SUCCESS / WRONG_PASSWORD

* send
    - ✅ não permitido pelo cliente se não está numa partida
    - ✅ peer_1 atualiza a posição [linha, coluna] do seu tabuleiro com my_symbol
    - ✅ peer_1 verifica se o tabuleiro já contém vitória
    - ✅ se houver vitória, peer_1 envia para o servidor:
        - ✅ MATCH_END
        - ✅ SUCCESS
        - ✅ username
        - ✅ opponent_username
        - ✅ winner_username
        - ✅ servidor atualiza score do vencedor
    - ✅ peer_1 envia para o peer_2:
        - ✅ SEND
        - ✅ tabuleiro serializado
    - ✅ peer_1 fica sem prompt enquanto não tem resposta
    - ✅ peer_2 atualiza seu próprio tabuleiro com payload
    - ✅ peer_2 verifica se o tabuleiro já contém vitória e exibe feedback na tela

## Bugs

- ✅ Quando peer desconecta com SIGINT o server morre
- Quando é a vez de peer 2, se peer 1 – que deveria estar on hold – der enter sem nada no prompt, o último comando digitado é lido novamente mas sem o primeiro char; ie, send vira end e a partida termina.
