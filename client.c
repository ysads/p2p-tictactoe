#include <poll.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>

#include "lib.h"
#include "libclient.h"

int main(int argc, char **argv)
{
    char command[MAX_COMMAND];
    char packet[MAX_PACKET];
    char *server_ip;

    // Initialize all fds to -1 to avoid indetermination in poll
    int listenfd = -1;
    int notifierfd = -1;
    int peerfd = -1;
    int serverfd = -1;

    int n_bytes, n_fds;
    int port;
    struct pollfd poll_fds[MAX_FDS];

    struct client_state client;
    client.is_playing = 0;
    client.is_logged_in = 0;
    client.is_prompt_blocked = 0;

    if (argc != 3)
    {
        fprintf(stderr, "Usage: %s <ServerIP> <ServerPort>\n", argv[0]);
        exit(1);
    }
    else
    {
        server_ip = argv[1];
        port = atoi(argv[2]);
    }

    listenfd = get_listenfd(port);
    notifierfd = create_notifier(listenfd, port, 5);
    serverfd = connect_to_host(server_ip, port);

    poll_fds[0].fd = listenfd;
    poll_fds[1].fd = notifierfd;
    poll_fds[2].fd = serverfd;
    poll_fds[3].fd = STDIN_FILENO;

    poll_fds[0].events = POLLIN;
    poll_fds[1].events = POLLIN;
    poll_fds[2].events = POLLIN;
    poll_fds[3].events = POLLIN;

    n_fds = 4;

    prompt();

    for (;;)
    {
        poll(poll_fds, n_fds, -1);

        for (int i = 0; i < n_fds; i++)
        {
            // Event of type "there is data to read"
            if (poll_fds[i].revents & POLLIN)
            {
                // Peer wants to connect
                if (poll_fds[i].fd == listenfd)
                {
                    if ((peerfd = accept(listenfd, (struct sockaddr *) NULL, NULL)) == -1)
                    {
                        perror("accept error\n");
                        exit(5);
                    }
                    else
                    {
                        poll_fds[n_fds].fd = peerfd;
                        poll_fds[n_fds].events = POLLIN;
                        n_fds++;
                    }
                }
                else if (poll_fds[i].fd == notifierfd) // Thread notified to send ping
                {
                    n_bytes = read(notifierfd, packet, MAX_PACKET);

                    if (client.is_playing)
                    {
                        send_packet(peerfd, PEER_PING_REQ, 0);
                        start_timer();
                    }
                }
                else if (poll_fds[i].fd == serverfd) // Server sent data
                {
                    n_bytes = read(serverfd, packet, MAX_PACKET);

                    if (n_bytes == 0) // Server disconnected
                    {
                        serverfd = -1;
                        poll_fds[2].fd = serverfd;
                    }
                    else
                    {
                        switch (get_packet_type(packet))
                        {
                            case LEADERS_RESP:
                            {
                                int n_records;
                                int n_bytes_unit = MAX_USERNAME + MAX_SCORE + 2;
                                char buffer[MAX_USERS * n_bytes_unit];
                                char n_str[3];

                                get_next_payload(packet, n_str);
                                n_records = atoi(n_str);

                                get_next_payload(NULL, buffer);

                                printf("Current scores:\n");
                                show_users_scores(buffer, n_records);
                                prompt();
                                break;
                            }
                            case LIST_RESP:
                            {
                                char *buffer;
                                char n_str[3];
                                int n_bytes_unit = MAX_USERNAME + INET_ADDRSTRLEN + 2;

                                get_next_payload(packet, n_str);
                                n_pool = atoi(n_str);
                                buffer = calloc(n_pool, n_bytes_unit);
                                get_next_payload(NULL, buffer);

                                save_users_list(buffer);
                                printf("There are %d users online.\n", n_pool);
                                show_users_list();
                                prompt();
                                break;
                            }
                            case LOGIN_RESP:
                            {
                                char status_str[2];
                                int status;

                                get_next_payload(packet, status_str);
                                status = atoi(status_str);

                                switch (status)
                                {
                                    case SUCCESS:
                                    {
                                        client.is_logged_in = 1;
                                        printf("You're logged in.\n");
                                        break;
                                    }
                                    case UNKNOWN_USER:
                                    {
                                        printf("You have to add yourself first.\n");
                                        break;
                                    }
                                    case WRONG_PASSWORD:
                                    {
                                        printf("Wrong password.\n");
                                        break;
                                    }
                                }
                                prompt();
                                break;
                            }
                            case PASSWD_RESP:
                            {
                                char status_str[2];
                                int status;

                                get_next_payload(packet, status_str);
                                status = atoi(status_str);

                                if (status == SUCCESS)
                                {
                                    printf("Password modified.\n");
                                }
                                else
                                {
                                    printf("You got your password wrong.\n");
                                }
                                prompt();
                            }
                            case PING_REQ:
                            {
                                send_packet(serverfd, PING_RESP, 0);
                                break;
                            }
                        }
                    }
                }
                else if (poll_fds[i].fd == STDIN_FILENO) // User entered command
                {
                    n_bytes = read(STDIN_FILENO, command, MAX_COMMAND);

                    // We won't do anything if prompt is blocked
                    if (client.is_prompt_blocked)
                    {
                        continue;
                    }

                    switch (get_command_type(command))
                    {
                        case C_ADDUSER:
                        {
                            char username[MAX_USERNAME];
                            char password[MAX_PASSWORD];

                            get_next_arg(username);
                            get_next_arg(password);
                            send_packet(serverfd, ADD_USER, 2, username, password);
                            printf("OK, you've been added.\n");
                            prompt();
                            break;
                        }
                        case C_BEGIN:
                        {
                            if (client.is_playing)
                            {
                                printf("Command not allowed when in a match.\n");
                                prompt();
                                break;
                            }

                            if (!client.is_logged_in)
                            {
                                printf("Command not allowed when not logged in.\n");
                                prompt();
                                break;
                            }

                            char username[MAX_USERNAME];
                            struct peer *peer;

                            get_next_arg(username);
                            get_peer_by_username(username, &peer);

                            if (peer == NULL)
                            {
                                printf("Peer not found. Type `list` to get a list of valid peers.\n");
                                prompt();
                                break;
                            }

                            peerfd = connect_to_host(peer->ip, port);
                            poll_fds[n_fds].fd = peerfd;
                            poll_fds[n_fds].events = POLLIN;
                            n_fds++;
                            send_packet(peerfd, BEGIN_REQ, 1, client.username);
                            printf("Wait for an answer...\n");
                            break;
                        }
                        case C_DELAY:
                        {
                            if (!client.is_playing)
                            {
                                printf("Command not allowed when not in a match.\n");
                                prompt();
                                break;
                            }
                            printf("Most recent latencies registered:\n");

                            for (int i = 0; i < 3; i++)
                            {
                                printf("%.4f milliseconds\n", latencies[i]);
                            }
                            prompt();
                            break;
                        }
                        case C_END:
                        {
                            if (!client.is_playing)
                            {
                                printf("Command not allowed when not in a match.\n");
                                prompt();
                                break;
                            }

                            send_packet(peerfd, END, 0);
                            send_packet(serverfd, MATCH_END,
                                4, S_FAILURE, client.username,
                                client.opponent_username, "FAILURE");
                            close(peerfd);
                            n_fds--;
                            client.is_playing = 0;
                            prompt();
                            break;
                        }
                        case C_EXIT:
                        {
                            if (client.is_playing)
                            {
                                printf("Command not allowed when in a match.\n");
                                prompt();
                                break;
                            }

                            if (client.is_logged_in)
                            {
                                send_packet(serverfd, LOGOUT, 0);
                            }
                            printf("Goodbye!\n");
                            exit(0);
                        }
                        case C_LEADERS:
                        {
                            send_packet(serverfd, LEADERS_REQ, 0);
                            printf("Wait for an answer...\n");
                            break;
                        }
                        case C_LIST:
                        {
                            send_packet(serverfd, LIST_REQ, 0);
                            printf("Wait for an answer...\n");
                            break;
                        }
                        case C_LOGIN:
                        {
                            if (client.is_logged_in)
                            {
                                printf("Command not allowed when logged in.\n");
                                prompt();
                                break;
                            }

                            char username[MAX_USERNAME];
                            char password[MAX_PASSWORD];

                            get_next_arg(username);
                            get_next_arg(password);
                            strcpy(client.username, username);
                            send_packet(serverfd, LOGIN_REQ, 2, username, password);
                            printf("Wait for the validation...\n");
                            break;
                        }
                        case C_LOGOUT:
                        {
                            if (client.is_playing)
                            {
                                printf("Command not allowed when in a match.\n");
                                prompt();
                                break;
                            }

                            if (!client.is_logged_in)
                            {
                                printf("Command not allowed when not logged in.\n");
                                prompt();
                                break;
                            }

                            client.is_logged_in = 0;
                            send_packet(serverfd, LOGOUT, 0);
                            printf("You logged out.\n");
                            prompt();
                            break;
                        }
                        case C_PASSWD:
                        {
                            if (!client.is_logged_in)
                            {
                                printf("Command not allowed when not logged in.\n");
                                prompt();
                                break;
                            }

                            char old_pw[MAX_PASSWORD];
                            char new_pw[MAX_PASSWORD];

                            get_next_arg(old_pw);
                            get_next_arg(new_pw);
                            send_packet(serverfd, PASSWD_REQ, 2, old_pw, new_pw);
                            printf("Wait for the validation...\n");
                            break;
                        }
                        case C_SEND:
                        {
                            if (!client.is_playing)
                            {
                                printf("Command not allowed when not in a match.\n");
                                prompt();
                                break;
                            }

                            int winner;
                            char row[2];
                            char col[2];
                            char serialized_board[10];

                            get_next_arg(row);
                            get_next_arg(col);

                            if (play_move(&client, row, col))
                            {
                                moves_played++;

                                winner = find_winner();
                                serialize_board(serialized_board);
                                send_packet(peerfd, SEND, 3, row, col, serialized_board);
                                print_board();

                                if (client.symbol == winner)
                                {
                                    printf("Congratulations! You are the winner of this match!\n");
                                    client.is_playing = 0;
                                    close(peerfd);
                                    n_fds--;
                                    send_packet(serverfd, MATCH_END,
                                        5, S_SUCCESS, client.username,
                                        client.opponent_username, client.username,
                                        "SUCCESS");
                                    prompt();
                                }
                                // We are not the winner and we have exhausted all moves, thus we have a tie */
                                else if (moves_played == MAX_MOVES)
                                {
                                    printf("Woosh, a tie! Maybe you should try harder next time.\n");
                                    client.is_playing = 0;
                                    close(peerfd);
                                    n_fds--;
                                    send_packet(serverfd, MATCH_END,
                                        5, S_SUCCESS, client.username,
                                        client.opponent_username, "TIE", "SUCCESS");
                                    prompt();
                                }
                                else
                                {
                                    printf("Wait for an answer...\n");
                                    client.is_prompt_blocked = 1;
                                }
                            }
                            else
                            {
                                printf("Invalid move. Try again.\n");
                                prompt();
                            }
                            break;
                        }
                        default:
                        {
                            printf("Command not allowed\n");
                            prompt();
                        }
                    }
                }
                else // peer sent data
                {
                    peerfd = poll_fds[MAX_FDS - 1].fd;
                    n_bytes = read(peerfd, packet, MAX_PACKET);

                    if (n_bytes == 0) // peer disconnected
                    {
                        if (client.is_playing)
                        {
                            close(peerfd);
                            n_fds--;
                            client.is_playing = 0;
                            client.is_prompt_blocked = 0;
                            send_packet(serverfd, MATCH_END,
                                4, S_PEER_DISCONNECTED, client.username,
                                client.opponent_username, "PEER_DISCONNECTED");
                            printf("\nOpponent crashed.\n");
                            prompt();
                        }
                    }
                    else
                    {
                        switch (get_packet_type(packet))
                        {
                            case BEGIN_REQ:
                            {
                                char ans;

                                printf("\nAccept? (y/n) ");
                                scanf("%c", &ans);
                                getchar();

                                if (ans == 'y')
                                {
                                    char opponent_username[MAX_USERNAME];

                                    client.is_playing = 1;
                                    client.is_prompt_blocked = 1;
                                    client.symbol = SYM_O;
                                    moves_played = 0;

                                    get_next_payload(packet, opponent_username);
                                    strcpy(client.opponent_username, opponent_username);
                                    send_packet(peerfd, BEGIN_RESP,
                                        3, S_SUCCESS, client.username, "SUCCESS");
                                    printf("You're in a match! Your symbol is `o`. Wait for your opponent.\n");
                                }
                                else
                                {
                                    send_packet(peerfd, BEGIN_RESP,
                                        2, S_FAILURE, "FAILURE");
                                    close(peerfd);
                                    n_fds--;
                                    prompt();
                                }
                                break;
                            }
                            case BEGIN_RESP:
                            {
                                char resp[2];

                                get_next_payload(packet, resp);

                                if (atoi(resp) == SUCCESS)
                                {
                                    char opponent_username[MAX_USERNAME];

                                    client.is_playing = 1;
                                    client.symbol = SYM_X;
                                    moves_played = 0;

                                    get_next_payload(NULL, opponent_username);
                                    strcpy(client.opponent_username, opponent_username);
                                    send_packet(serverfd, MATCH_BEGIN,
                                        2, client.username, opponent_username);
                                    init_board();
                                    print_board();
                                    printf("You're in a match. Your symbol is `x`. Make your move.\n");
                                }
                                else
                                {
                                    printf("Your opponent didn't accept...\n");
                                    close(peerfd);
                                    n_fds--;
                                }
                                prompt();
                                break;
                            }
                            case END:
                            {
                                close(peerfd);
                                n_fds--;
                                client.is_playing = 0;
                                printf("Opponent left the match.\n");
                                prompt();
                                break;
                            }
                            case PEER_PING_REQ:
                            {
                                send_packet(peerfd, PEER_PING_RESP, 0);
                                break;
                            }
                            case PEER_PING_RESP:
                            {
                                register_latency();
                                break;
                            }
                            case SEND:
                            {
                                char row[2];
                                char col[2];
                                char updated_board[10];

                                client.is_prompt_blocked = 0;
                                moves_played++;

                                get_next_payload(packet, row);
                                get_next_payload(NULL, col);
                                get_next_payload(NULL, updated_board);

                                update_board(updated_board);

                                // The winner can only be the other player
                                if (find_winner())
                                {
                                    printf("%s won! Maybe next time?\n", client.opponent_username);
                                    client.is_playing = 0;
                                    close(peerfd);
                                    n_fds--;
                                }
                                else if (moves_played == MAX_MOVES)
                                {
                                    printf("Woosh, a tie! Maybe you should try harder next time.\n");
                                    client.is_playing = 0;
                                    close(peerfd);
                                    n_fds--;
                                }
                                else
                                {
                                    printf("Your turn!\n");
                                }
                                print_board();
                                prompt();
                                break;
                            }
                        }
                    }
                }
            }
        }
    }
    exit(0);
}
