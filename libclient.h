// Limits
#define MAX_COMMAND         30
#define MAX_FDS             5

// Tic-tac-toe constants
#define MAX_MOVES           9
#define SYM_EMPTY           0
#define SYM_O               4
#define SYM_X               1
#define WINNER_O            12 // 3 times `4` in a row
#define WINNER_X            3  // 3 times `1` in a row

// User command codes
#define C_ADDUSER           0x01
#define C_BEGIN             0x02
#define C_DELAY             0x03
#define C_END               0x04
#define C_EXIT              0x05
#define C_LEADERS           0x06
#define C_LIST              0x07
#define C_LOGIN             0x08
#define C_LOGOUT            0x09
#define C_PASSWD            0x0A
#define C_SEND              0x0B
#define C_UNKNOWN           0x0C

struct client_state {
    int is_logged_in;
    int is_playing;
    int is_prompt_blocked;
    char opponent_username[MAX_USERNAME];
    char username[MAX_USERNAME];
    int symbol;
};

struct peer {
    char username[MAX_USERNAME];
    char ip[INET_ADDRSTRLEN];
    int is_playing;
};

extern int n_pool;
extern double latencies[3];
extern int board[3][3];
extern int moves_played;

int find_winner();
void get_next_arg(char *);
uchar get_command_type(char *);
void get_peer_by_username(char *, struct peer **);
void init_board();
int play_move(struct client_state *, char *, char *);
void print_board();
void prompt();
void register_latency();
void save_users_list(char *);
void serialize_board(char *);
void show_users_list();
void show_users_scores(char *, int);
void start_timer();
void update_board(char *);
