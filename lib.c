#include <pthread.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <arpa/inet.h>

#include "lib.h"

void add_padding(char **str, int max_n)
{
    int init = strlen(*str);

    for (int i = 0; i < max_n - init - 1; i++)
    {
        strcat(*str, " ");
    }
}

int connect_to_host(char *ip, int port)
{
    int fd;
    struct sockaddr_in host_addr;

    if ((fd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        fprintf(stderr, "socket error\n");
        exit(2);
    }

    bzero(&host_addr, sizeof(host_addr));
    host_addr.sin_family = AF_INET;
    host_addr.sin_port = htons(port);

    if (inet_pton(AF_INET, ip, &host_addr.sin_addr) <= 0)
    {
        fprintf(stderr, "inet_pton error for %s\n", ip);
        exit(3);
    }

    if (connect(fd, (struct sockaddr *) &host_addr, sizeof(host_addr)) < 0)
    {
        fprintf(stderr, "connect error\n");
        exit(4);
    }

    return fd;
}

int create_notifier(int listenfd, int port, int seconds)
{
    int threadfd;
    struct notification_args args;
    args.port = port;
    args.seconds = seconds;

    pthread_t id;
    pthread_create(&id, NULL, notify, &args);
    threadfd = accept(listenfd, NULL, NULL);

    return threadfd;
}

int get_listenfd(int port)
{
    int listenfd;
    int reuse = 1;
    struct sockaddr_in servaddr;

    if ((listenfd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
        perror("socket error\n");
        exit(2);
    }

    setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(reuse));

    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port = htons(port);

    if (bind(listenfd, (struct sockaddr *)&servaddr, sizeof(servaddr)) == -1)
    {
        perror("bind error\n");
        exit(3);
    }

    if (listen(listenfd, MAX_LISTEN_QUEUE) == -1)
    {
        perror("listen error\n");
        exit(4);
    }
    return listenfd;
}

void get_next_payload(char *packet, char *payload)
{
    char ch;
    static int ptr = 0;
    static char *packet_ref;

    if (packet != NULL)
    {
        packet_ref = packet;
        ptr = strlen(packet_ref) + 1; // After first occurence of 0x00;
    }

    int i = 0;

    for (; (ch = packet_ref[ptr]) != PACKET_SEPARATOR; i++, ptr++)
    {
        payload[i] = packet_ref[ptr];
    }
    payload[i] = '\0';
    ptr++;
}

uchar get_packet_type(char *packet)
{
    return packet[0];
}

char *get_type_str(uchar type)
{
    switch (type)
    {
        case LEADERS_RESP:
        {
            return "LEADERS-RESP";
        }
        case LIST_RESP:
        {
            return "LIST-RESP";
        }
        case LOGIN_RESP:
        {
            return "LOGIN-RESP";
        }
        case PASSWD_RESP:
        {
            return "PASSWD-RESP";
        }
        case PING_REQ:
        {
            return "PING-REQ";
        }
        case ADD_USER:
        {
            return "ADD-USER";
        }
        case LEADERS_REQ:
        {
            return "LEADERS-REQ";
        }
        case LIST_REQ:
        {
            return "LIST-REQ";
        }
        case LOGIN_REQ:
        {
            return "LOGIN-REQ";
        }
        case LOGOUT:
        {
            return "LOGOUT";
        }
        case MATCH_BEGIN:
        {
            return "MATCH-BEGIN";
        }
        case MATCH_END:
        {
            return "MATCH-END";
        }
        case PASSWD_REQ:
        {
            return "PASSWD-REQ";
        }
        case PING_RESP:
        {
            return "PING-RESP";
        }
        case BEGIN_REQ:
        {
            return "BEGIN-REQ";
        }
        case BEGIN_RESP:
        {
            return "BEGIN-RESP";
        }
        case END:
        {
            return "END";
        }
        case PEER_PING_REQ:
        {
            return "PEER-PING-REQ";
        }
        case PEER_PING_RESP:
        {
            return "PEER-PING-RESP";
        }
        case SEND:
        {
            return "SEND";
        }
    }
    return "";
}

void *notify(void *args)
{
    int port = ((struct notification_args *) args)->port;
    int seconds = ((struct notification_args *) args)->seconds;
    struct timespec req = {seconds, 0};
    struct timespec rem;
    int ownfd = connect_to_host("127.0.0.1", port);

    for (;;)
    {
        nanosleep(&req, &rem);
        write(ownfd, "heartbeat", 10);
    }
    return NULL;
}

void send_packet(int fd, uchar type, int n_args, ...)
{
    int len = n_args + 2;
    int arg_len, *args_lens;
    uchar *packet;
    char *type_str;
    char *current_arg;
    va_list args_list;

    type_str = get_type_str(type);
    len += strlen(type_str);

    args_lens = malloc(n_args * sizeof(int));
    va_start(args_list, n_args);

    for (int i = 0; i < n_args; i++)
    {
        arg_len = strlen(va_arg(args_list, char *));
        args_lens[i] = arg_len;
        len += arg_len;
    }

    va_end(args_list);
    va_start(args_list, n_args);

    packet = malloc(len);
    packet[0] = type;

    int p = 1;

    for (int i = 0; i < strlen(type_str); i++)
    {
        packet[p] = type_str[i];
        p++;
    }

    packet[p] = PACKET_SEPARATOR;
    p++;

    for (int i = 0; i < n_args; i++)
    {
        current_arg = va_arg(args_list, char *);

        for (int j = 0; j < args_lens[i]; j++)
        {
            packet[p] = current_arg[j];
            p++;
        }
        packet[p] = PACKET_SEPARATOR;
        p++;
    }

    va_end(args_list);
    write(fd, packet, len);
    free(args_lens);
    free(packet);
}
