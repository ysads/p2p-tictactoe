#include <poll.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>

#include "lib.h"
#include "libserver.h"

int main(int argc, char **argv)
{
    char packet[MAX_PACKET + 1];

    // Initialize all fds to -1 to avoid indetermination in poll
    int clientfd = -1;
    int listenfd = -1;
    int notifierfd = -1;

    int n_bytes, n_fds;
    int port;
    struct pollfd *poll_fds;

    if (argc != 2)
    {
        fprintf(stderr, "Usage: %s <Port>\n", argv[0]);
        exit(1);
    }
    else
    {
        port = atoi(argv[1]);
    }

    log_event(SERVER_STARTED, 0);
    signal(SIGINT, log_and_shutdown);
    listenfd = get_listenfd(port);
    notifierfd = create_notifier(listenfd, port, 5);

    poll_fds = malloc(MAX_CLIENTS * sizeof *poll_fds);
    poll_fds[0].fd = listenfd;
    poll_fds[1].fd = notifierfd;
    poll_fds[0].events = POLLIN;
    poll_fds[1].events = POLLIN;

    n_fds = 2;

    for (;;)
    {
        poll(poll_fds, n_fds, -1);

        for (int i = 0; i < n_fds; i++)
        {
            // Event of type "there is data to read"
            if (poll_fds[i].revents & POLLIN)
            {
                // Client wants to connect
                if (poll_fds[i].fd == listenfd)
                {
                    char remote_ip[INET_ADDRSTRLEN];

                    if ((clientfd = accept(listenfd, NULL, NULL)) == -1)
                    {
                        perror("accept error\n");
                        exit(5);
                    }
                    else
                    {
                        get_ip(clientfd, remote_ip);

                        poll_fds[n_fds].fd = clientfd;
                        poll_fds[n_fds].events = POLLIN;
                        n_fds++;

                        client_init(clientfd, remote_ip);
                        log_event(CLIENT_CONNECTED, 1, remote_ip);
                    }
                }
                else if (poll_fds[i].fd == notifierfd) // Thread notified to send heartbeats
                {
                    int fd;
                    n_bytes = read(notifierfd, packet, MAX_PACKET);

                    for (int i = 0; i < n_clients; i++)
                    {
                        fd = clients[i].fd;
                        clients[i].n_pings_unanswered++;

                        if (clients[i].n_pings_unanswered > MAX_UNANSWERED_PINGS)
                        {
                            printf("Lost connection with client %s.\n", clients[i].ip);
                        }
                        else
                        {
                            send_packet(fd, PING_REQ, 0);
                        }
                    }
                }
                else // Connected client sent packet
                {
                    clientfd = poll_fds[i].fd;
                    n_bytes = read(clientfd, packet, MAX_PACKET);

                    // Client disconnected
                    if (n_bytes == 0)
                    {
                        struct client *client;

                        close(poll_fds[i].fd);
                        poll_fds[i] = poll_fds[n_fds - 1];
                        n_fds--;

                        get_client(clientfd, &client);

                        if (client->is_logged_in)
                        {
                            n_logged--;
                        }
                        client_remove(clientfd);
                        log_event(CLIENT_DISCONNECTED, 1, client->ip);
                    }
                    else // There is data on socket
                    {
                        switch (get_packet_type(packet))
                        {
                            case ADD_USER:
                            {
                                char user[MAX_USERNAME];
                                char pw[MAX_PASSWORD];

                                get_next_payload(packet, user);
                                get_next_payload(NULL, pw);
                                add_user(user, pw);
                                break;
                            }
                            case LEADERS_REQ:
                            {
                                int n_bytes_unit = MAX_USERNAME + MAX_SCORE + 2;
                                char buffer[MAX_USERS * n_bytes_unit];
                                char n_str[3];

                                stringify_leaders(buffer, n_str);
                                send_packet(clientfd, LEADERS_RESP, 2, n_str, buffer);
                                break;
                            }
                            case LIST_REQ:
                            {
                                int n_bytes_unit = MAX_USERNAME + INET_ADDRSTRLEN + 2;
                                char n_str[3];
                                char *buffer = calloc(n_logged, n_bytes_unit);

                                sprintf(n_str, "%d", n_logged);

                                if (n_logged > 0)
                                {
                                    stringify_logged_users(buffer);
                                    send_packet(clientfd, LIST_RESP, 2, n_str, buffer);
                                }
                                else
                                {
                                    send_packet(clientfd, LIST_RESP, 1, n_str);
                                }
                                free(buffer);
                                break;
                            }
                            case LOGIN_REQ:
                            {
                                char user[MAX_USERNAME];
                                char pw[MAX_PASSWORD];
                                char ip[INET_ADDRSTRLEN];
                                int status;

                                get_ip(clientfd, ip);
                                get_next_payload(packet, user);
                                get_next_payload(NULL, pw);
                                status = verify_user(user, pw);

                                if (status == SUCCESS)
                                {
                                    client_login(clientfd, user, pw);
                                    send_packet(clientfd, LOGIN_RESP,
                                        2, S_SUCCESS, "SUCCESS");
                                    log_event(CLIENT_LOGIN, 3, user, ip, "SUCCESS");
                                }
                                else if (status == UNKNOWN_USER)
                                {
                                    send_packet(clientfd, LOGIN_RESP,
                                        2, S_UNKNOWN_USER, "UNKNOWN-USER");
                                    log_event(CLIENT_LOGIN,
                                        4, user, ip, "UNKNOWN-USER", "FAILURE");
                                }
                                else if (status == WRONG_PASSWORD)
                                {
                                    send_packet(clientfd, LOGIN_RESP,
                                        2, S_WRONG_PASSWORD, "WRONG-PASSWORD");
                                    log_event(CLIENT_LOGIN,
                                        4, user, ip, "WRONG-PASSWORD", "FAILURE");
                                }
                                break;
                            }
                            case LOGOUT:
                            {
                                struct client *client;

                                get_client(clientfd, &client);
                                client_logout(client);
                                log_event(CLIENT_LOGOUT, 2, client->username, client->ip);
                                break;
                            }
                            case MATCH_BEGIN:
                            {
                                char username_1[MAX_USERNAME];
                                char username_2[MAX_USERNAME];
                                struct client *client_1, *client_2;

                                get_next_payload(packet, username_1);
                                get_next_payload(NULL, username_2);
                                get_client_by_username(username_1, &client_1);
                                get_client_by_username(username_2, &client_2);
                                client_1->is_playing = 1;
                                client_2->is_playing = 1;
                                strcpy(client_1->opponent_ip, client_2->ip);
                                strcpy(client_2->opponent_ip, client_1->ip);
                                strcpy(client_1->opponent_username, username_2);
                                strcpy(client_2->opponent_username, username_1);
                                log_event(MATCH_STARTED,
                                    4, username_1, client_1->ip, username_2, client_2->ip);
                                break;
                            }
                            case MATCH_END:
                            {
                                int status;
                                char status_str[2];
                                char username_1[MAX_USERNAME];
                                char username_2[MAX_USERNAME];
                                char username_winner[MAX_USERNAME];
                                struct client *client_1, *client_2;

                                get_next_payload(packet, status_str);
                                status = atoi(status_str);

                                get_next_payload(NULL, username_1);
                                get_client_by_username(username_1, &client_1);
                                client_1->is_playing = 0;

                                get_next_payload(NULL, username_2);

                                if (status == PEER_DISCONNECTED)
                                {
                                    log_event(MATCH_ENDED,
                                        5, username_1, client_1->ip,
                                        username_2, client_1->opponent_ip, "PEER DISCONNECTED");
                                    break;
                                }

                                get_client_by_username(username_2, &client_2);
                                client_2->is_playing = 0;

                                if (status == SUCCESS)
                                {
                                    get_next_payload(NULL, username_winner);

                                    if (strcmp(username_winner, "TIE") == 0)
                                    {
                                        update_score(username_1, SCORE_TIE);
                                        update_score(username_2, SCORE_TIE);
                                        log_event(MATCH_ENDED,
                                            6, username_1, client_1->ip,
                                            username_2, client_2->ip, "TIE",
                                            "SUCCESS");
                                    }
                                    else
                                    {
                                        update_score(username_winner, SCORE_WIN);
                                        log_event(MATCH_ENDED,
                                            7, username_1, client_1->ip,
                                            username_2, client_2->ip, "WINNER",
                                            username_winner, "SUCCESS");
                                    }
                                }
                                else
                                {
                                    log_event(MATCH_ENDED,
                                        5, username_1, client_1->ip,
                                        username_2, client_2->ip, "FAILURE");
                                }
                                break;
                            }
                            case PASSWD_REQ:
                            {
                                char old_password[MAX_PASSWORD];
                                char new_password[MAX_PASSWORD];
                                struct client *client;

                                get_next_payload(packet, old_password);
                                get_client(clientfd, &client);

                                if (verify_user(client->username, old_password) == SUCCESS)
                                {
                                    get_next_payload(NULL, new_password);
                                    update_password(client, new_password);
                                    send_packet(clientfd, PASSWD_RESP,
                                        2, S_SUCCESS, "SUCESS");
                                }
                                else
                                {
                                    send_packet(clientfd, PASSWD_RESP,
                                        2, S_WRONG_PASSWORD, "WRONG PASSWORD");
                                }
                                break;
                            }
                            case PING_RESP:
                            {
                                for (int i = 0; i < n_clients; i++)
                                {
                                    if (clients[i].fd == clientfd)
                                    {
                                        clients[i].n_pings_unanswered = 0;
                                    }
                                }
                                break;
                            }
                        }
                    }
                }
            }
        }
    }
    exit(0);
}
